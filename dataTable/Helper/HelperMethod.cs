﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace dataTable.Helper
{
    public static class HelperMethod
    {
        public static string KarakterDuzelt(string metin)
        {
            metin = metin.ToLower();
            metin = metin.Replace("İ", "I");
            metin = metin.Replace("ı", "i");
            metin = metin.Replace("Ğ", "G");
            metin = metin.Replace("ğ", "g");
            metin = metin.Replace("Ö", "O");
            metin = metin.Replace("ö", "o");
            metin = metin.Replace("Ü", "U");
            metin = metin.Replace("ü", "u");
            metin = metin.Replace("Ş", "S");
            metin = metin.Replace("ş", "s");
            metin = metin.Replace("Ç", "C");
            metin = metin.Replace("ç", "c");
            metin = metin.Replace(" ", "-");
            metin = metin.Replace("ş", "s");
            metin = metin.Replace("Ş", "s");
            metin = metin.Replace("İ", "i");
            metin = metin.Replace("I", "i");
            metin = metin.Replace("ı", "i");
            metin = metin.Replace("ö", "o");
            metin = metin.Replace("Ö", "o");
            metin = metin.Replace("ü", "u");
            metin = metin.Replace("Ü", "u");
            metin = metin.Replace("Ç", "c");
            metin = metin.Replace("ç", "c");
            metin = metin.Replace("ğ", "g");
            metin = metin.Replace("Ğ", "g");
            metin = metin.Replace("---", "-");
            metin = metin.Replace("--", "-");
            metin = metin.Replace("?", "");
            metin = metin.Replace("/", "");
            metin = metin.Replace(".", "");
            metin = metin.Replace("'", "");
            metin = metin.Replace("#", "");
            metin = metin.Replace("%", "");
            metin = metin.Replace("&", "");
            metin = metin.Replace("*", "");
            metin = metin.Replace("!", "");
            metin = metin.Replace(",", "");
            metin = metin.Replace("@", "");
            metin = metin.Replace("+", "");
            metin = metin.Replace(";", "");
            metin = metin.Replace(":", "");
            metin = metin.Replace("(", "");
            metin = metin.Replace(")", "");
            metin = metin.Replace("[", "");
            metin = metin.Replace("]", "");
            metin = metin.Replace("{", "");
            metin = metin.Replace("}", "");
            metin = metin.Replace("°", "");
            metin = metin.Replace("^", "");
            metin = metin.Replace("~", "");
            metin = metin.Replace("£", "");
            metin = metin.Replace("$", "");
            metin = metin.Replace("₺", "");
            metin = metin.Replace("½", "");
            metin = metin.Replace("€", "");
            metin = metin.Replace("|", "");
            metin = metin.Replace("<", "");
            metin = metin.Replace(">", "");
            metin = metin.Replace("\"", "");
            return metin;
        }
    }
}