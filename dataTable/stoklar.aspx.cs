﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace dataTable
{
    public partial class stoklar : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "dataTable()", true);
                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                sb.Append("<script language=\"javascript\">");
                sb.Append("document.getElementById(\"mnStoklar\").className=\"active\"");
                sb.Append("</script>");
                ClientScript.RegisterStartupScript(this.GetType(), "MyScript", sb.ToString());

                if (StokKontrol())
                {
                    pnlStoklar.Visible = true;
                    pnlStokYok.Visible = false;
                    StokDoldur();
                }
                else
                {
                    pnlStoklar.Visible = false;
                    pnlStokYok.Visible = true;
                }
            }
        }
        private bool StokKontrol()
        {
            bool kisiVarmi = false;
            using (dataTableDBEntities ent = new dataTableDBEntities())
            {
                var bilgiler = (from v in ent.STOK
                                select v).ToList();
                if (bilgiler.Count > 0)
                    kisiVarmi = true;
            }
            return kisiVarmi;
        }
        public void StokDoldur()
        {
            using (dataTableDBEntities ent = new dataTableDBEntities())
            {
                var bilgiler = (from v in ent.STOK
                                select new { v.StokId, v.BarkodNo, v.StokAdi, v.KritikSeviyeUyarisi, v.KritikStokSeviyesi }).ToList();

                rptStoklar.DataSource = bilgiler;
                rptStoklar.DataBind();
            }
        }
        protected void StokSil(object sender, EventArgs e)
        {
            RepeaterItem item = (sender as LinkButton).NamingContainer as RepeaterItem;
            int id = int.Parse((item.FindControl("stkId") as Label).Text);
            lblstokid.Text = id.ToString();
            lblStokAdi.Text = (item.FindControl("lblAd") as Label).Text;

            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "onay()", true);
            popUp.Visible = false;

        }
        string giren="10", cikan="3";
        protected void rptStoklar_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            Label stokId = (Label)e.Item.FindControl("stkId");
            Label kritikSeviyeUyarisi = (Label)e.Item.FindControl("lblKritikSeviyeUyarisi");
            Label kritikStokSeviyesi = (Label)e.Item.FindControl("lblKritikStokSeviyesi");
            Label birim = (Label)e.Item.FindControl("lblStokBirimi");
            var span1 = e.Item.FindControl("spUyari") as HtmlGenericControl;
            var spanOkay = e.Item.FindControl("spOkay") as HtmlGenericControl;
            if (kritikSeviyeUyarisi.Text == "1")
            {
                int id = int.Parse(stokId.Text);
                int stokSeviyesi = int.Parse(kritikStokSeviyesi.Text);
                int Giren = Convert.ToInt32(giren);
                int Cikan = Convert.ToInt32(cikan);
                int Kalan = Giren - Cikan;
                if (stokSeviyesi == Kalan)
                {
                    span1.Attributes["style"] = "color: #FCA311;";
                    span1.Attributes["title"] = "Ürün stoğu belirlediğiniz kritik stok seviyesine düşmüştür. Kalan miktar: " + Kalan.ToString() + " " + birim.Text;
                    span1.Visible = true;
                }
                else if (Kalan < stokSeviyesi)
                {
                    span1.Attributes["style"] = "color: #d00000;";
                    span1.Attributes["title"] = "Ürün stoğu belirlediğiniz kritik stok seviyesinin altına düşmüştür. Kalan miktar: " + Kalan.ToString() + " " + birim.Text;
                    span1.Visible = true;
                }
                else
                {
                    spanOkay.Attributes["style"] = "color: #70e000;";
                    spanOkay.Attributes["title"] = "Ürün stoğu belirlediğiniz kritik stok seviyesinin üzerindedir.";
                    spanOkay.Visible = true;
                }
            }
            else
            {
                span1.Visible = false;
            }
        }
    }
}