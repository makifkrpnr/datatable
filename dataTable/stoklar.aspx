﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="stoklar.aspx.cs" Inherits="dataTable.stoklar" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <title>hesaptutar.com | Stoklar</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphSiteMap" runat="server">
    <div class="breadcrumb-area">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/anasayfa">Genel Bakış</a></li>
                <li class="breadcrumb-item active" aria-current="page">Stoklar</li>
            </ol>
        </nav>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="muhasebe">
        <asp:UpdatePanel ID="UpdatePanel4" UpdateMode="Conditional" runat="server">
            <ContentTemplate>
                <asp:Panel ID="pnlStokYok" runat="server">
                    <div class="content">
                        <div class="container">
                            <div class="row">
                                <h4>Stoklar</h4>
                                <span>Henüz bir stok kaydı yok. Yeni bir tane ekleyerek devam edebilirsiniz.</span>
                            </div>
                            <div class="row mt-5">
                                <div class="col-lg-4 yeniStok">
                                    <div class="icon mb-3"></div>
                                    <a href="/stoklar/yeni-stok" class="buton-link">
                                        <button type="button" class="buton mb-2">
                                            <span>Yeni Stok Oluştur</span>
                                        </button>
                                    </a>
                                </div>
                                <div class="col-lg-4 stokGrubu">
                                    <div class="icon mb-3"></div>
                                    <a href="/ayarlar/kategori-grup-ayarlari" class="buton-link">
                                        <button type="button" class="buton mb-2">
                                            <span>Stok Grubu Oluştur</span>
                                        </button>
                                    </a>
                                </div>
                                <div class="col-lg-4 stokAyarları">
                                    <div class="icon mb-3"></div>
                                    <a href="/ayarlar/stok-ayarlari" class="buton-link">
                                        <button type="button" class="buton mb-2">
                                            <span>Stok Ayarları</span>
                                        </button>
                                    </a>
                                </div>
                            </div>

                        </div>
                    </div>
                </asp:Panel>
            </ContentTemplate>
        </asp:UpdatePanel>

    </div>
    <asp:Panel ID="pnlStoklar" runat="server">
        <div class="content musteriler">
            <div class="container">
                <h5 class="table-header">Stoklar</h5>
                <span class="table-info">Bu alanda sisteme kayıtlı olan tüm stok kartları görüntülenmektedir.</span>
                <asp:Label ID="lblID" runat="server" Visible="false" Text="Label"></asp:Label>
                <a href="/stoklar/yeni-stok">
                    <button type="button" class="buton add">Stok Ekle<span>&nbsp<i class="fas fa-user-plus"></i></span></button>
                </a>
                <asp:UpdatePanel ID="UpdatePanel3" runat="server" RenderMode="Block" UpdateMode="Conditional" ChildrenAsTriggers="false">
                    <ContentTemplate>
                        <table id="stoklar" class="table nowrap" width="100%">
                            <thead>
                                <tr>
                                    <th>Stok Adı</th>
                                    <th>Barkod No</th>
                                    <th>Kategori</th>
                                    <th>Tedarikçi</th>
                                    <th class="row-edit-header">Durum</th>
                                    <th class="row-edit-header">İşlem</th>
                                </tr>
                            </thead>
                            <tbody>
                                <asp:Repeater ID="rptStoklar" runat="server" OnItemDataBound="rptStoklar_ItemDataBound">
                                    <ItemTemplate>
                                        <tr>
                                            <td class="tasmayan">
                                                <asp:Label ID="lblAd" Text='<%# Eval("StokAdi") %>' CssClass="byk" runat="server"></asp:Label>
                                                 
                                            </td>
                                            <td onclick="location.href='/stoklar/<%# dataTable.Helper.HelperMethod.KarakterDuzelt(Eval("StokAdi").ToString()) %>-<%# Eval("StokId") %>'"><asp:Label ID="stkId" Visible="false" Text='<%# Eval("StokId") %>' runat="server"></asp:Label><%# Eval("StokId") %><%# Eval("BarkodNo") %></td>
                                            
                                            <td onclick="location.href='/stoklar/<%# dataTable.Helper.HelperMethod.KarakterDuzelt(Eval("StokAdi").ToString()) %>-<%# Eval("StokId") %>'">Elektronik</td>
                                            <td onclick="location.href='/stoklar/<%# dataTable.Helper.HelperMethod.KarakterDuzelt(Eval("StokAdi").ToString()) %>-<%# Eval("StokId") %>'">hepsiburada</td>
                                            <td>
                                                <span id="spUyari" class="spUyari" runat="server" data-bs-toggle="tooltip" data-bs-placement="bottom" visible="false"><i class="fas fa-exclamation-circle"></i></span>
                                                <span id="spOkay" class="spUyari" runat="server" data-bs-toggle="tooltip" data-bs-placement="bottom" visible="false"><i class="fas fa-check-circle"></i></span>
                                                
                                                <asp:Label ID="lblStokBirimi" Visible="false" runat="server" Text="Adet"></asp:Label>
                                                <asp:Label ID="lblKritikSeviyeUyarisi" Visible="false" runat="server" Text='<%# Eval("KritikSeviyeUyarisi") %>'></asp:Label>
                                                <asp:Label ID="lblKritikStokSeviyesi" Visible="false" runat="server" Text='<%# Eval("KritikStokSeviyesi") %>'></asp:Label>
                                            </td>
                                            <td class="row-edit">
                                                <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
                                                    <ContentTemplate>
                                                        <span>
                                                            <asp:LinkButton ID="lbSil" runat="server" OnClick="StokSil"><span class="sil"><i class="fas fa-trash-alt"></i></span></asp:LinkButton></span>
                                                    </ContentTemplate>
                                                    <Triggers>
                                                        <asp:AsyncPostBackTrigger ControlID="lbSil" EventName="Click" />
                                                    </Triggers>
                                                </asp:UpdatePanel>
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </tbody>
                        </table>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
    </asp:Panel>

    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div class="confirm-area" id="sil">
                <div class="confirm-hide-area">
                    <span class="icon"><i class="fas fa-exclamation"></i></span>
                    <asp:Label ID="lblstokid" Visible="false" runat="server" Text="Label"></asp:Label>

                    <span class="uyari">
                        <b>
                            <asp:Label ID="lblStokAdi" runat="server" Text="Label"></asp:Label></b> isimli stok kartını silmek üzeresiniz,<br />
                        bu işlem geri alınamaz.
                <br>
                        Devam etmek istiyor musunuz?</span>
                    <div class="button-area mt-3">
                        <asp:LinkButton ID="lbStokSil" class="buton" runat="server">Evet</asp:LinkButton><br>
                        <button type="button" class="buton light donustur">Hayır</button>
                        <asp:UpdateProgress ID="UpdateProgress2" runat="server">
                            <ProgressTemplate>
                                <div runat="server" id="loading">
                                    <span>
                                        <i class="fas fa-cog fa-spin"></i>
                                        <asp:Label ID="Label1" runat="server" Text="İşleminiz gerçekleştiriliyor..."></asp:Label></span>
                                </div>
                            </ProgressTemplate>
                        </asp:UpdateProgress>
                    </div>
                </div>
            </div>
            <div class="popup" runat="server" id="popUp" visible="false">
                <span>
                    <i class="far fa-thumbs-up" runat="server" id="basarili"></i>
                    <i class="far fa-thumbs-down" runat="server" id="basarisiz"></i>
                    <asp:Label ID="lblMesaj" runat="server" Text="Label"></asp:Label></span>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>

    <script>
        function pageLoad () {
            $('.spUyari').tooltip('enable')
        }
    </script>
    <script>
        function onay() {
            $(document).ready(function () {
                $("#sil").addClass("show");
                $(".overlay.confirm").addClass("show");

                $("#sil .buton.light").click(function () {
                    $("#sil").removeClass("show");
                    $(".overlay.confirm").removeClass("show");
                });
                $('*').click(function (e) {
                    if (!$(e.target).is('#sil') && !$(e.target).is('#sil *') && !$(e.target).is('#yeniStokGrubu *') && !$(e.target).is('#guncelleStokGrubu *')
                        && !$(e.target).is('#yeniStokBirimi *') && !$(e.target).is('#guncelleStokBirimi *') && !$(e.target).is('#yeniDepo *')
                        && !$(e.target).is('#guncelleDepo *')) {
                        $("#sil").removeClass("show");
                        $(".overlay.confirm").removeClass("show");
                    }
                });
            });
        }
        function stoksildevam() {
            $(".overlay.confirm").removeClass("show");
        }
    </script>

    <!-- Datatables -->
    <script>
        function dataTable() {
            $.extend($.fn.dataTable.defaults, {
                responsive: true
            });
            $('#stoklar').DataTable({
                stateSave: true,
                "stateSaveParams": function (settings, data) {
                    data.search.search = "";
                },
                dom: 'Blfrtip',
                language: {
                    paginate: {
                        next: '<i class="fas fa-chevron-right"></i>',
                        previous: '<i class="fas fa-chevron-left"></i>'
                    }
                },
                buttons: [{
                    extend: 'copyHtml5',
                    titleAttr: 'Kopyala',
                    exportOptions: {
                        columns: [1, 2, 3, 4]
                    }
                }, {
                    extend: 'excelHtml5',
                    titleAttr: 'Excel',
                    title: 'Stok Listesi',
                    exportOptions: {
                        columns: [1, 2, 3, 4]
                    }
                }, {
                    extend: 'csvHtml5',
                    titleAttr: 'CSV',
                    title: 'Stok Listesi',
                    exportOptions: {
                        columns: [1, 2, 3, 4]
                    }
                }, {
                    extend: 'pdfHtml5',
                    titleAttr: 'PDF',
                    download: 'open',
                    title: 'Stok Listesi',
                    exportOptions: {
                        columns: [1, 2, 3, 4]
                    },
                    customize: function (doc) {
                        doc.content[1].table.widths =
                            Array(doc.content[1].table.body[0].length + 1).join('*').split('');
                    }
                }],
                "pageLength": 10,
            });
        }
    </script>
</asp:Content>